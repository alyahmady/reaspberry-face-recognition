# USAGE
# python pi_face_recognition.py

# import the necessary packages
from datetime import datetime
from PIL import Image
from imutils.video import VideoStream
from imutils.video import FPS
import face_recognition
import argparse
import telegram
import imutils
import pickle
import time
import cv2
import os

# change the directory to the right path of working scripts
# !IMPORTANT put yout project folder instead of PROJECT_DIR
working_directory = "PROJECT_DIR"
os.chdir(working_directory)
working_directory = os.getcwd()

# initialize telegram bot
# !IMPORTANT put yout bot token below instead of TOKEN
# !IMPORTANT put yout group chat ID below instead of CHAT_ID
bot = telegram.Bot(token='TOKEN')
group_chat_id = 'CHAT_ID'

# load the known faces and embeddings along with OpenCV's Haar
# cascade for face detection
print("[INFO] loading encodings + face detector...")
data = pickle.loads(open("encodings.pickle", "rb").read())
detector = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")

# initialize the video stream and allow the camera sensor to warm up
print("[INFO] starting video stream...")
vs = VideoStream(usePiCamera=True).start()
time.sleep(2.0)

# start the FPS counter
fps = FPS().start()

# loop over frames from the video file stream
while True:
	try:
		# grab the frame from the threaded video stream and resize it
		# to 500px (to speedup processing)
		frame = vs.read()
		frame = imutils.resize(frame, width=500)
		
		# convert the input frame from (1) BGR to grayscale (for face
		# detection) and (2) from BGR to RGB (for face recognition)
		gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

		# detect faces in the grayscale frame
		rects = detector.detectMultiScale(gray, scaleFactor=1.1, 
			minNeighbors=5, minSize=(30, 30),
			flags=cv2.CASCADE_SCALE_IMAGE)

		# OpenCV returns bounding box coordinates in (x, y, w, h) order
		# but we need them in (top, right, bottom, left) order, so we
		# need to do a bit of reordering
		boxes = [(y, x + w, y + h, x) for (x, y, w, h) in rects]

		# compute the facial embeddings for each face bounding box
		encodings = face_recognition.face_encodings(rgb, boxes)

		# loop over the facial embeddings
		for encoding in encodings:
			# try to match each face in the input image to our known encodings
			matches = face_recognition.compare_faces(data["encodings"],
				encoding)
			name = "unknown"

			# check to see if we have found a match
			if True in matches:
				# find the indexes of all matched faces then initialize a
				# dictionary to count the total number of times each face
				# was matched
				matchedIdxs = [i for (i, b) in enumerate(matches) if b]
				counts = {}

				# loop over the matched indexes and maintain a count for each recognized face face
				for i in matchedIdxs:
					name = data["names"][i]
					counts[name] = counts.get(name, 0) + 1

				# determine the recognized face with the largest number
				# of votes (note: in the event of an unlikely tie Python
				# will select first entry in the dictionary)
				name = max(counts, key=counts.get)

				print("DOOR OPENED")
				""" YOU SHOULD CALL THE DOOR OPENING FUNCTION HERE """
				
				clean_current_date = datetime.now().strftime(f"%Y/%m/%d %H:%M:%S")
				bot.send_message(
					chat_id=group_chat_id,
					text=f"کاربر {name} در تاریخ: {clean_current_date} درب را باز کرد."
				)
			else:
				# produce a file name based on the current date and time
				current_date = datetime.now().strftime(f"%Y_%m_%d_%H_%M_%S")
				file_name = f"captured_{current_date}.jpg"

				# change working directory to sent_photos folder
				os.chdir(f"{working_directory}/sent_photos")

				# save a picture from the current frame in a file
				# to send via telegram for security reasons
				image_written = cv2.imwrite(file_name, frame)
				if image_written:
					# send telegram photo
					bot.send_photo(
						chat_id=group_chat_id,
						photo=open(f"{working_directory}/sent_photos/{file_name}", 'rb')
					)
				
				# send telegram message containing the current date and time
				clean_current_date = datetime.now().strftime(f"%Y/%m/%d %H:%M:%S")
				bot.send_message(
					chat_id=group_chat_id,
					text="فردی با تصویر ارسال شده در بالا قصد بازکردن درب را داشت." \
						 f"در تاریخ: {clean_current_date}")
				# get back to the working directory
				os.chdir(working_directory)
			


		# update the FPS counter
		fps.update()

	# if user enterd Ctrl + C, process will break and finish recording
	# but as I mentioned in documents, you should do it in cron job
	# only if you are using it on a raspberry pi
	except KeyboardInterrupt:
		break

# stop the timer and display FPS information
fps.stop()

# do a bit of cleanup
cv2.destroyAllWindows()
vs.stop()
