# COMMENT
* STEP DESCRIPTION
* "FILE COMMAND LINE"
- COMMAND
>>> PYTHON COMMANDS


====== Botting PiCamera ======

* connect PiCamera to RaspberryPi   # https://bit.ly/35brTuA
* enable PiCamera in Raspbian       # https://bit.ly/2ubbppA





====== Starting Python Virtual Environment ======
- run the below commands
- cd ~
- git clone https://gitlab.com/better.aly.ahmady/reaspberry-face-recognition.git
- gcd reaspberry-face-recognition
- gpython3 -m venv venv              # https://bit.ly/2QwV5qs
- gvenv/bin/activate






====== Testing Camera ======

- raspistill -o ~/Desktop/image.jpg
- raspivid -o ~/Desktop/video.h264





====== Expand filesystem to install OpenCV ======

- sudo raspi-config
* navigate: Advanced Options -> Expand FileSystem
- sudo reboot




====== Some commands to avoid errors ======

- sudo nano /etc/dphys-swapfile

* find line `CONF_SWAPSIZE=100` and change 100 with 1024

- sudo /etc/init.d/dphys-swapfile stop
- sudo /etc/init.d/dphys-swapfile start

* run command below to test new size of swap
- free -m

- sudo raspi-config

* navigate: Boot Options -> Desktop / CLI -> Console Autologin
* rerun command below:
- sudo raspi-config

* navigate: Advanced Options -> Memory Split
* enter `64` in prompt
- sudo reboot





====== Installing dependencies in venv ======

* update, upgrade and install cmake
- sudo apt-get update
- sudo apt-get upgrade
- sudo apt-get install build-essential cmake unzip  pkg-config

* graphical libraries
- sudo apt-get install libjpeg-dev libpng-dev libtiff-dev
- sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
- sudo apt-get install libxvidcore-dev libx264-dev
- sudo apt-get install libgtk-3-dev
- sudo apt-get install libboost-all-dev
- sudo apt-get install libhdf5-dev libhdf5-serial-dev libhdf5-100
- sudo apt-get install libqtgui4 libqtwebkit4 libqt4-test python3-pyqt5
- sudo apt-get install libatlas-base-dev gfortran
- sudo apt-get install libjasper-dev

* python 3 and pip
- sudo apt-get install python3 python3-dev python3-pip

* to make sure if pip is installed
- wget https://bootstrap.pypa.io/get-pip.py
- sudo python3 get-pip.py

* python libraries
- sudo pip install opencv-contrib-python==4.1.0.25
- sudo pip install numpy scipy scikit-image 
- pip install dlib
- pip install face_recognition
- pip install imutils
- sudo apt-get install python-picamera python3-picamera
- sudo pip install picamera
- sudo pip install python-telegram-bot --upgrade




====== Testing libraries ======

# Should not get any error
# lines with >>> are python statements

- python3
>>> import cv2
>>> cv2.__version__
>>> import imutils




====== Return back to last swap size !IMPORTANT ======
* run command below
- sudo nano /etc/dphys-swapfile

* find line `CONF_SWAPSIZE=100` and change 1024 with 100
- sudo /etc/init.d/dphys-swapfile stop
- sudo /etc/init.d/dphys-swapfile start

* command below to test new size of swap
- free -m

* run command below
- sudo raspi-config

* navigate: Boot Options => Desktop / CLI => Desktop Autologin
* rerun command below:
- sudo raspi-config

* navigate: Advanced Options => Memory Split
* enter `16` in prompt
- sudo reboot




====== Put telegram information in script ======

* create a bot in telegram
* create a group and give the bot and yourself, the admin role
* give permissions to bot for sending messages in group
* open `pi_face_recognition.py` with a text editor
* put `TOKEN OF YOUR BOT` in line 25 instead of TOKEN
* put `YOUR GROUP CHAT ID` in line 26 instead of CHAT_ID



====== Door opening functions or methods ======

* If it's only about learning purposes, skip this section
* Put the door script `open` method in line 92



====== Encode and specify faces for running script ======

* We should run this command each time we put a new folder (person pictures) in dataset folder
- sudo python3 ~/reaspberry-face-recognition/encode_faces.py
 



====== Schedule task to run on Raspbian Startup ======

* make a new file in the home directory (~) called `face_detector_runner.sh`

* open it and put below lines in it (without double quotes)

* "#!/bin/sh"
* "sleep 10"
* "python3 ~/reaspberry-face-recognition/pi_face_recognition.py"

* change access mode of *.sh script using this command
- sudo chmod u+x ~/face_detector_runner.sh

* Then run below commands
- crontab -e
* Select `nano`

* type below line in the opened editor (without double quotes)
* "@reboot ~/face_detector_runner.sh &"
- sudo reboot

